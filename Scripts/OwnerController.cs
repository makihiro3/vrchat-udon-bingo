﻿using UdonSharp;
using UnityEngine;
using VRC.SDKBase;

namespace Bingo.Scripts
{
    public class OwnerController : UdonSharpBehaviour
    {
        [SerializeField] public GameObject ownershipControlled;
        [SerializeField] public GameObject[] activateButtons;

        void Start()
        {
            var active = Networking.IsOwner(ownershipControlled);
            Active(active);
        }

        public void OnTakeOwner()
        {
            Networking.SetOwner(Networking.LocalPlayer, ownershipControlled);
            Active(true);
        }

        public override void OnOwnershipTransferred(VRCPlayerApi _player)
        {
            var active = Networking.IsOwner(ownershipControlled);
            Active(active);
        }

        private void Active(bool active)
        {
            foreach (var b in activateButtons)
            {
                b.SetActive(active);
            }
        }
    }
}