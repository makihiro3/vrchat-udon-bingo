using UdonSharp;
using UnityEngine;
using UnityEngine.UI;
using VRC.SDKBase;

namespace Bingo.Scripts
{
    public class TakeOwner : UdonSharpBehaviour
    {
        public GameObject target;
        public GraphicRaycaster raycaster;
        public Material materialActive;
        public Material materialStandby;
        public Renderer marker;

        public override void OnPickup()
        {
            Networking.SetOwner(Networking.LocalPlayer, target);
            raycaster.enabled = true;
            marker.material = materialActive;
        }

        public override void OnOwnershipTransferred(VRCPlayerApi _player)
        {
            if (Networking.IsOwner(gameObject)) return;
            raycaster.enabled = false;
            marker.material = materialStandby;
        }
    }
}