﻿using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;

namespace Bingo.Scripts
{
    [UdonBehaviourSyncMode(BehaviourSyncMode.Manual)]
    public class GlobalController : UdonSharpBehaviour
    {
        public GameObject[] targets;

        [UdonSynced, FieldChangeCallback(nameof(State))]
        private uint _state;

        private uint State
        {
            get => _state;
            set
            {
                _state = value;
                if (Networking.IsOwner(gameObject))
                {
                    RequestSerialization();
                }

                var x = value;
                foreach (var target in targets)
                {
                    x ^= x << 13;
                    x ^= x >> 17;
                    x ^= x << 5;
                    var t = (UdonBehaviour)target.GetComponent(typeof(UdonBehaviour));
                    t.SetProgramVariable("state", x);
                    t.SendCustomEvent("BingoReset");
                }
            }
        }

        private void Start()
        {
            if (State != 0) return;
            if (!Networking.IsOwner(gameObject)) return;
            BingoReset();
        }

        public void BingoReset()
        {
            var x = (uint)UnityEngine.Random.Range(0, 4294967295);
            for (int i = 0; i < 128; i++)
            {
                x ^= x << 13;
                x ^= x >> 17;
                x ^= x << 5;
            }

            State = x;
        }
    }
}