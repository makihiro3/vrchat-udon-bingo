using UdonSharp;
using UnityEngine.UI;
using VRC.SDKBase;
using VRC.Udon.Common.Interfaces;

namespace Bingo.Scripts
{
    public class CanvasBingoboard : UdonSharpBehaviour
    {
        public uint state;

        [UdonSynced, FieldChangeCallback(nameof(Draw))]
        private int _draw;

        private int Draw
        {
            get => _draw;
            set
            {
                _draw = value;
                if (Networking.IsOwner(gameObject))
                {
                    RequestSerialization();
                }

                for (int i = 0; i < texts.Length; i++)
                {
                    texts[i].enabled = i < Draw;
                }
            }
        }

        public Text[] texts;

        public void BingoReset()
        {
            var a = new int[texts.Length];
            // Fisher-Yates shuffle
            // https://en.wikipedia.org/wiki/Fisher-Yates_shuffle
            for (int i = 0; i < a.Length; ++i)
            {
                // xorshift32
                state ^= state << 13;
                state ^= state >> 17;
                state ^= state << 5;
                var j = state - (i + 1) * (state / (i + 1));
                if (j != i) a[i] = a[j];
                a[j] = i + 1;
            }

            for (int i = 0; i < a.Length; i++)
            {
                texts[i].text = a[i].ToString();
                texts[i].enabled = false;
            }

            Draw = 0;
        }

        public void BingoToggle()
        {
            if (!Networking.IsOwner(gameObject))
            {
                SendCustomNetworkEvent(NetworkEventTarget.Owner, nameof(BingoToggle));
                return;
            }

            Draw++;
        }
    }
}