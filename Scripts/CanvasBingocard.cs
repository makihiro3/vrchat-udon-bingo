using UdonSharp;
using UnityEngine.UI;
using VRC.SDKBase;

namespace Bingo.Scripts
{
    [UdonBehaviourSyncMode(BehaviourSyncMode.Manual)]
    public class CanvasBingocard : UdonSharpBehaviour
    {
        public uint state;

        public Text[] texts;
        public Toggle[] toggles;

        [UdonSynced, FieldChangeCallback(nameof(State))]
        private uint _state;

        private uint State
        {
            set
            {
                _state = value;
                if (Networking.IsOwner(gameObject))
                {
                    RequestSerialization();
                }

                var x = value;
                for (int i = toggles.Length - 1; i >= 0; i--)
                {
                    toggles[i].isOn = (x & 1u) != 0u;
                    x >>= 1;
                }
            }
        }

        public void BingoReset()
        {
            var a = new int[5];
            Shuffle(a, 0, 1, 5);
            Shuffle(a, 5, 16, 5);
            Shuffle(a, 10, 31, 4);
            Shuffle(a, 14, 46, 5);
            Shuffle(a, 19, 61, 5);
            State = 0;
        }

        private void Shuffle(int[] a, int idx, int offset, int m)
        {
            // Reservoir sampling(Simple Algorithm)
            // https://en.wikipedia.org/wiki/Reservoir_sampling
            for (int i = 0; i < m; ++i)
                a[i] = i + offset;
            for (int i = m; i < 15; ++i)
            {
                state ^= state << 13;
                state ^= state >> 17;
                state ^= state << 5;
                var j = state - (i + 1) * (state / (i + 1));
                if (j < m)
                    a[j] = i + offset;
            }

            for (int i = 0; i < m; i++)
            {
                toggles[i + idx].isOn = false;
                texts[i + idx].text = a[i].ToString();
            }
        }

        public void BingoToggle()
        {
            uint x = 0;
            for (int i = 0; i < toggles.Length; i++)
            {
                x <<= 1;
                x |= (toggles[i].isOn ? 1u : 0u);
            }

            State = x;
        }
    }
}